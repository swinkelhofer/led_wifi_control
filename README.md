# LED WiFi Control

## What you need

Here are a few suggestions, be free too choose any alternative you like to

| **Description** | **Image/Link** |
| ----------  | ------- |
| **LED strips** | <a href="https://www.amazon.de/gp/product/B07DZQZ5JV"><img src="img/ledstrips.jpg" width="200"></a> |
| **DC/DC Step-Down Module** | <a href="https://www.amazon.de/gp/product/B01MQGMOKI"><img src="img/stepdown.jpg"  width="200"></a> |
| **NodeMCU Lua Lolin V3 ESP8266 ESP-12E Module** | <a href="https://www.amazon.de/gp/product/B074Q27ZBQ"><img src="img/nodemcu.jpg"  width="200"></a> |
| **Transistor** | <a href="https://www.amazon.de/Transistor-92-Epitaktischen-Silizium-Transistoren-Schwarz/dp/B01D9JDGN6"><img src="img/transistor.jpg"  width="200"></a> |


## Hardware

<img src="img/schematic.png">

 * Remove switch from LED strip power cable
 * Solder the power cable onto the power input of the step-down module (be aware of the right polarity)
 * Adjust the output of the step-down module to 5V (use a small screwdriver for the potentionmeter on the board)
 * Connect the outputs of the step-down module to GND(-) and 3V(+) pins of the ESP8266 module
 * Connect pin D6 to the base pin of the transistor
 * Connect input(+) of the LED strip to input(+) of the step-down module
 * Connect input(-) of the LED strip to collector pin of the transistor
 * Connect emitter pin of the transistor to input(-) of the step-down module

If everything is in place, the circuit should be able to control the LED strip (on/off and brightness) via pin D6 of the ESP8266 module. 