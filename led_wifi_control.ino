#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char* ssid      = "<SSID of your WIFI>";
const char* password  = "<PASSWORD for your WIFI>";

unsigned int port = 2390;

char packetBuffer[255];

WiFiUDP Udp;

const int PIN = 12; // GPIO 12 bzw. D6
const int BOARD_LED = 0;

void setup()
{
  // set pin modes
  pinMode(PIN, OUTPUT);
  pinMode(BOARD_LED, OUTPUT);

  digitalWrite(PIN, HIGH);
  delay(300);
  digitalWrite(PIN, LOW);
  delay(300);
  digitalWrite(BOARD_LED, HIGH);
  delay(300);
  digitalWrite(BOARD_LED, LOW);

  // begin serial and connect to WiFi
  Serial.begin(115200);
  delay(100);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Udp.begin(port);

}
int num = 0;
int current = 0;

void loop()
{
  Serial.println(num);
  digitalWrite(BOARD_LED, HIGH);
  delay(300);
  digitalWrite(BOARD_LED, LOW);
  int packetSize = Udp.parsePacket();
  if(packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remoteIp = Udp.remoteIP();
    Serial.print(remoteIp);
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }    
    Serial.println("Contents:");
    Serial.println(packetBuffer);
    Serial.write(packetBuffer);
    Serial.println();

    // TEMP parse data from packet
    Serial.print("Received \"");
    Serial.print(packetBuffer);
    Serial.println("\"");

    num = atoi(packetBuffer);
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    if(num == 0) {
      current = 0;
      analogWrite(PIN, 0);
      Udp.write("0");
    }
    else if (num == -1) {
      char s_num[10];
      sprintf(s_num, "%d", current);
      Udp.write(s_num);
    }
    else if (num <= PWMRANGE) {
      current = num;
      analogWrite(PIN, num);
      char s_num[10];
      sprintf(s_num, "%d", current);
      Udp.write(s_num);
    }
    else {
      current = PWMRANGE;
      analogWrite(PIN, PWMRANGE);
      char s_num[10];
      sprintf(s_num, "%d", PWMRANGE);
      Udp.write(s_num);
    }
    Udp.endPacket();
  }


}
